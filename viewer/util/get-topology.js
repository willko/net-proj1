'use strict';
module.export = (function (){
var data;

try{
  data = require('../../data/.topology.json');
}catch(err){
  throw new Error('\n\n' + '[!] Error: You must generate the topology.json file first!' +
                  '\n\n' + '[*] Try running net-proj1 with -x flag to generate it.' +
                  '\n'   + '[*] Pass it the topology text file you wish to use.' +
                  '\n'   + '[*] The duration arg can be 0.001 (the value doesn\'t matter).' +
                  '\n\n' + '[!] ' + err.toString());
}

return data;
}());