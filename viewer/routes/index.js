'use strict';

var express = require('express');
var router = express.Router();
var data = require('../util/get-topology');


/* Main Page. */
router.get('/', function (req, res) {
  res.render('index', { title: 'Topology' });
});

/* Data Route */
router.get('/topology', function (req, res){
  res.send(data);
});



module.exports = router;
