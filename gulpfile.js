'use strict';

var gulp = require('gulp')
  , sh = require('shelljs');



//list of files to watch
var src_files = [
        'src/*.js'
      , 'src/**/*.js'
      , 'gulpfile.js'
      , 'package.json'
];

//cmd: gulp watch
//watch for file changes and trigger run task on change
gulp.task('watch', function (){
  gulp.watch(src_files, ['run']);
});

//cmd: gulp run
//run the program with the default topology1.txt file for 10 seconds
gulp.task('run', function (){
  //fancy convergence
  sh.exec('node src/driver.js data/topology1.txt 10.0 -df' , {silent: false});

  //fancy, first 2 packets arrive
  //sh.exec('node src/driver.js data/topology1.txt 0.06 -df' , {silent: false});
});

gulp.task('compress', function(){
  sh.exec('mkdir -p net-proj1');
  sh.exec('cp -r ./src ./net-proj1');
  sh.exec('cp -r ./bin ./net-proj1');
  sh.exec('cp -r ./data ./net-proj1');
  sh.exec('cp package.json ./net-proj1');
  sh.exec('cp README.txt ./net-proj1');
  sh.exec('cp README-FIRST.pdf ./net-proj1');
  sh.exec('cp RESULTS.pdf ./net-proj1');
  sh.exec('tar czvf net-proj1.tar.gz ./net-proj1', {silent: false});
  sh.exec('rm -r net-proj1');
});

gulp.task('scp', ['compress'], function(){
  sh.exec('scp ./net-proj1.tar.gz wko4@zeus.cs.txstate.edu:~/', {silent: false});
});

//cmd: gulp export
//copies needed files, compresses them, then scp's them to txstate server
gulp.task('export', ['scp'], function(){
  sh.exec('rm ./net-proj1.tar.gz');
});

//cmd: gulp
//the default task that triggers watch and run tasks
gulp.task('default', ['watch', 'run']);
gulp.task('test', ['run']);