# net-proj1

**DESCRIPTION:**

  Distance Vector Networking Simulator.  Runs a simulation of a network
  that uses distance vectors (not using Dijkstra's algorithm) and monitors
  routing tables of nodes on the network until convergence has occurred
  between all nodes. Also allows testing of DATA packet routes & link
  failures.

## RUN:

```
    npm install --production
```

Takes a topology filename & duration specifying how
long to run the simulation in seconds:

```
    node ./src/driver.js ./data/topology1.txt 10.555
```

**Project Structure:**

```
    - 'bin' directory:
        └── net-proj1

    - 'src' directory:
        ├── driver.js
        ├── init
        │   ├── arg-parser.js
        │   ├── index.js
        │   └── topology-writer.js
        ├── network
        │   ├── config.js
        │   └── index.js
        ├── packet
        │   ├── index.js
        │   └── packet-handler.js
        ├── router-tables
        │   ├── index.js
        │   ├── routers.js
        │   ├── trigger-update.js
        │   └── update.js
        └── util
            ├── logger.js
            └── p-queue.js

    - 'data' directory:
        ├── events
        │   ├── data-packet-1.json
        │   ├── data-packet-2.json
        │   ├── data-packet-3.json
        │   ├── link-failure-1.json
        │   ├── link-failure-2.json
        │   └── link-failure-3.json
        ├── top1_sol.txt
        ├── topology1.txt
        ├── topology2.txt
        ├── topology3.txt
        └── topology4.txt

    - README.md
```

**HELP MENU & OPTIONS:**

  There are quite a few options I have baked in. Pass the
  `-h` flag to the program to view the menu.

```
      Available Options:
      --------------------------------------------
        -c, --color              show fancy tables in color
        -d, --debug              show debug (all) output
        -f, --fancy              print fancy router tables
        -h, --help               show this menu
        -i, --inject             insert events defined in a json file,
                                 must be followed by a filename or path
        -v, --verbose            show verbose (medium) output
```
