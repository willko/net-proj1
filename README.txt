CS4310
William Olson
11/04/2015


                      net-proj1: Networks Project One



README.txt:
  This file is meant to help ease the process of running the program.
  Hints are denoted with [*], important notes and problems are marked [!].


  Project Files Included:

    - 'bin' directory:
        └── net-proj1

    - 'src' directory:
        ├── driver.js
        ├── init
        │   ├── arg-parser.js
        │   ├── index.js
        │   └── topology-writer.js
        ├── network
        │   ├── config.js
        │   └── index.js
        ├── packet
        │   ├── index.js
        │   └── packet-handler.js
        ├── router-tables
        │   ├── index.js
        │   ├── routers.js
        │   ├── trigger-update.js
        │   └── update.js
        └── util
            ├── logger.js
            └── p-queue.js

    - 'data' directory:
        ├── events
        │   ├── data-packet-1.json
        │   ├── data-packet-2.json
        │   ├── data-packet-3.json
        │   ├── link-failure-1.json
        │   ├── link-failure-2.json
        │   └── link-failure-3.json
        ├── top1_sol.txt
        ├── topology1.txt
        ├── topology2.txt
        ├── topology3.txt
        └── topology4.txt

    - README.txt
    - README-FIRST.pdf
    - RESULTS.pdf


DOCUMENTATION:
---------------------------------------------------------------------------------------

SETUP:

  Transfer files to a txstate server (preferably zeus).
  First check if the server has node installed. You can
  do this like so.

  In the terminal type the following lines.

    #get node version
    node -v

    #get npm version
    npm -v

  Node should return the version number. I tested with version v0.10.36.
    [!] If node is not installed please find a server with 'node' or 'nodejs'
    [!] installed on it. The zeus.cs.txstate.edu should have it installed.
    [*] Node is easy to install if it comes to that. Visit: http://nodejs.org
    [*] Or for simple ubuntu installs: 'sudo apt-get install nodejs' works.
    [*]   Note: this will install as 'nodejs', so to use 'node' just create a
    [*]   symbolic link like so: 'sudo ln -sf /usr/bin/nodejs /usr/bin/node'

  Once you have a system with it installed, you can run the program.
  In the terminal, enter the following line and press enter.

HOW_TO_RUN:

    #for normal node installs
    node ./src/driver.js ./data/topology1.txt 10.555

    #OR for nodejs installs:
    nodejs ./src/driver.js ./data/topology1.txt 10.555


HELP_MENU_&_OPTIONS:

  There are quite a few options I have baked in. Pass the
  -h flag to the program to view the menu.

      Available Options:
      --------------------------------------------
        -c, --color              show fancy tables in color
        -d, --debug              show debug (all) output
        -f, --fancy              print pretty router tables
        -h, --help               show this menu
        -i, --inject             insert events defined in a json file,
                                 must be followed by a filename or path
        -v, --verbose            show verbose (medium) output


  [*] In order to use the --fancy option, you must run an npm install
      from the same directory that the package.json file is in.

        #install dependencies defined in package.json
        npm install --production


FILES_OVERVIEW:

  Each folder in 'src' directory pretty much creates a single module,
  except for 'util', which is a normal folder that contains 2 modules.

  There are 6 total main modules:
    - network
    - init
    - router-tables
    - Packet
    - p-queue
    - logger

  The src/driver.js file is just a simple runner that interfaces
  with the modules. The bin/net-proj1 file is an executable that
  can be ran without using the node command, and is linked to the
  src/driver.js file. (It is used for global installs and gets
  linked to the user's node folder when "npm install -g ./" is
  ran. You should not have to worry about it though as you wont
  be installing this package globally). Also note "sudo" is needed
  if you want to try global installations of npm packages.

  Each module file/files are well commented. Please dive into their
  respective files to read more about them.


DATA_STRUCTURES:

  config:
    This object is used as a sort of shared container
    that is needed by many modules.  It holds the clock
    time as well as the links data structure (noted below)
    and a few helpful methods for displaying/debugging. Also
    it holds the duration value from the passed in console and
    statistical variables that accumulate data during execution.
    Last but not least it holds the enums for Packet Types.
    config = {
        clock: val
      , duration: val
      , step:  val
      , step_count: val
      , action_count: val
      , links: {object}
      , EVENT_TYPES: {
          DV_PACKET: enum_val
        , DATA_PACKET: enum_val
        , LINK_FAILURE: enum_val
      }
      , get_delay: [function]
      , log_links: [function]
    }

    (note: some stat variables are
     missing here, see src code for
     complete config struct)

  links:
    This holds all the data as read in from the file
    only as an array (each array member is 1 line in file).
    Each line read in gets split into src, dest, cost, & hop
    fields.  This struct is used for setting neighbors of
    each node in the routers data struct (noted below) and
    for determining the estimated time of arrival for packet
    events that get added to the event queue.
      links = [ { src: '4', dest: '1', cost: 532, delay: 0.076792222 },
                { src: '4', dest: '2', cost: 669, delay: 0.133467327 },
                { src: '3', dest: '2', cost: 722, delay: 0.051783073 },
                { src: '3', dest: '4', cost: 196, delay: 0.184216793 },
                { src: '0', dest: '1', cost: 907, delay: 0.125563734 },
                { src: '0', dest: '2', cost: 291, delay: 0.217457185 },
                { src: '1', dest: '3', cost: 24, delay: 0.208844158 } ]


  routers:
    The object structure that holds
    the routing-tables of all routers.
    Accessed through the router-tables
    module. The structure is as follows.
     routers = {
        router_id0: {
          table: [
              {dest: val, cost: val, hop: val}
            , {dest: val, cost: val, hop: val}
          ],
          neighbors: [ val, val, val, ...etc ]
        },
        router_id1: {
          table: [
              {dest: val, cost: val, hop: val}
            , {dest: val, cost: val, hop: val}
            , {dest: val, cost: val, hop: val}
          ],
          neighbors: [ val, val, val, ...etc ]
        }
        //... etc.
     }
     *Where router_id# are
      the names of the nodes.
      These are stored as the
      keys within the routers
      object. The value for each
      key is an object {} which
      holds a single key/value pair.
      This key/value pair is the table
      for that router (an array).  Each
      table array holds an object for
      each row in the router's table.
      The neighbors is another property
      that belongs to each node in the
      routers object. The value of the
      neighbors key is an array of strings
      which contain node ids.

   Packet:
     The packet data structure is used
     to represent all packet types and
     is synonymous with an event. Since
     packets are considered events they
     are directly added to the priority
     queue and processed straight from
     the queue. An example packet looks
     something like the following.
      packet = {
        id: val,
        type: enum_val,
        eta: val,
        header: {
            src:     val,
            og_src:  val,
            dest:    val,
            og_dest: val
        },
        payload:{
          data: val,
          table: [
              { dest: val, cost: val }
            , { dest: val, cost: val }
            // ... etc.
          ]
        }
        __proto__: {
          //protoypes have tons of default stuff
          //all js object have a __proto__ property
          process: [function]
        }
      }
    Each packet also has a prototype
    method (process) which is called
    when it is time to process the
    packet. Note that prototype functions
    are inherited from the initial Packet
    constructor object, but the "this"
    variable in prototype functions refer to
    the individual Object calling the method.
    The method itself is only created once
    and shared through inheritance for
    efficiency.

    p-queue:
      The p-queue data struct is a module
      itself.  It acts as an interface
      for the priority queue.  Also
      reffered to as the event queue,
      this data structure is just a simple
      array that get sorted every time a
      new object is added. (lazy implementation)
      I was going to do an insert method but
      instead of running sort every time an
      element is added but never got around to it.
      The performace is not so bad that optimization
      was a must.  The following stucture defined in
      the p-queue module that holds the events is:
        event_queue = []; //dynamic array
      The p-queue module itself is stuctured in the
      following wayL
        p-queue = {
            push: [function]
          , pop: [function]
          , get_events: [function]
        }

        (note: the p-queue module can be found in the util folder)

ALTERNATIVE_WAYS_TO_RUN:

  #straight from driver
  ./driver.js file.txt 8

  #use the linked runner
  ./bin/net-proj1 file.txt 9

  #use package.json's defined scripts
  npm run test1
  npm run test2
  npm run test3
  #...etc: there are 6 total tests (1 for each question)

  #inject some events (manually)
  node ./src/driver.js ./data/topology1.txt 10 -i ./data/events/data-packet-1.json


NOTES:

  Feel free to contact me if you have any questions. I will be happy to help.
  willko747@gmail.com, wko4@txstate.edu


KNOWN_ISSUES:

  I did notice on rare occasions that the program would quit prematurely
  before all output was printed (usually on large topologies that spit out
  over 4000 lines of output).  This can be fixed by piping the output to cat
  like so...

  # assure all output shown
  ./src/driver.js ./data/topology1.txt 10 | cat

  ..or by redirecting output to a file...

  # output to file
  ./src/driver.js ./data/topology1.txt 10 > output.txt

END
