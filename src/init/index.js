/********************************
*********************************

  Program:   Project One (CS4310)
  Author:    William Olson
  Date:      11/04/2015

  File:      init (init/index.js)

    This file is an initializer utility
    for setting up the network topology
    and initializing the router-tables.

*******************************
*******************************/

'use strict';


var config = require('../network/config')
  , arg_parser = require('./arg-parser')
  , router_tables = require('../router-tables')
  , Packet = require('../packet')
  , p_queue = require('../util/p-queue')
  , logger = require('../util/logger');


module.exports = function () {

  /*
        ----------------------------
              DEFINE HELPERS
        ----------------------------
  */

 /**
   * structure_input:
   *   helper function to clean up file data
   *   and build the structured 'links' object
   *   array.  Gets passed as a callback to the
   *   native javascript forEach function.
   **/
  var structure_input = function (item, index, arr) {
    var link = {}
      , tmp_arr;

    //trim whitespace from ends
    arr[index] = item.trim();

    //condense excess whitespace and tabs to just 1 space
    //side-note: the below expression uses regular expression
    // checking that searches for 1 or more spaces OR a tab
    // character, and replaces what it finds with a space.
    arr[index] = item.replace(/[ ]+|\t/g, ' ');

    //splits each row (line in file) into an Array
    //where each field is a member in the array
    tmp_arr = arr[index].split(' ');

    try{
      //create link from data
      link.src = tmp_arr[0];
      link.dest = tmp_arr[1];
      link.cost = parseInt(tmp_arr[2]);
      link.delay = parseFloat(tmp_arr[3]);
    }catch(err){
      throw new Error('[!] Error: A problem occurred mutating the file data');
    }
    arr[index] = link;
  };


 /**
   * trigger_update:
   *   schedules the first dv_packets to be
   *   sent. This function is passed to the
   *   native Javascript Array's forEach method.
   *   It must have the nodes array and index i
   *   in scope when being called.
   *   The array that the forEach is called upon
   *   is a list of all the neighbors for the node
   *   nodes[i].
   * @param: node
   *   the Object which is a table entry in nodes[i]'s
   *   router table. Only neighbors of nodes[i] are
   *   passed in.
   *
   * note: this function is used only one time (in
   *       this program).  There is another official
   *       trigger_update method in the router-tables
   *       module (router-tables/trigger-update.js).
   **/
  var trigger_update = function (node) {
    logger.log(logger.LVL.CHATTY, '[*] Scheduling *First trigger update from Node ' + nodes[i] +
                ' to Node ' + node.dest + ' (@time: ' + config.clock + ')');
    //create a new event (trigger_update) and add it to the queue
    var dv_packet = new Packet(config.EVENT_TYPES.DV_PACKET, nodes[i], node.dest,
                                     config.get_delay(nodes[i], node.dest),
                                     {
                                         data: 'TRIGGER_UPDATE'
                                       , og_src: nodes[i]
                                       , og_dest: node.dest
                                       , table: router_tables.get_dv_table(nodes[i], node.dest)
                                     });
    p_queue.push(dv_packet);
  };


  /*
        ----------------------------
            HANDLE CONSOLE ARGS
        ----------------------------
  */

  var input = arg_parser.get_input();
  var file_data = input.file_data;
  config.opts = input.opts;

  //bail if help menu requested
  if(input.opts.help) return;

  //make each line of file_data an object
  file_data.forEach(structure_input);

  //set other config options
  config.duration = input.duration;
  config.links = file_data;

  //export link_data if requested (debug feature)
  if(config.opts.export_json)
    require('./topology-writer')(config.links);

  //inject external events into the queue
  if(config.opts.inject_data){
    var injected_packet;
    for(var n=0; n < config.opts.inject_data.length; n++){
      //decorate with prototypes
      injected_packet = new Packet(config.opts.inject_data[n]);
      p_queue.push(injected_packet);
    }
  }

  /*
        ----------------------------
            INIT ROUTER TABLES
        ----------------------------
  */

  //set initial router_tables
  for(var i = 0; i < config.links.length; i++){
    router_tables.update(config.links[i].src, [config.links[i]], config.links[i].dest, config.clock);

    //don't forget dest to src links (assuming all nodes know their own neighbors)
    //create a reverse src/dest object and run the update for other end of the link
    var reverse_link = {};
    reverse_link.src = config.links[i].dest;
    reverse_link.dest = config.links[i].src;
    reverse_link.cost = config.links[i].cost;
    router_tables.update(reverse_link.src, [reverse_link], reverse_link.dest, config.clock);
  }

  //add fixed neighbors to router-tables
  var nodes = router_tables.get_nodes();
  for(i=0; i < nodes.length; i++)
    router_tables.add_neighbors(nodes[i], router_tables.get_table(nodes[i]));


  /*
        ----------------------------
         RUN FIRST TRIGGER_UPDATES
        ----------------------------
  */

  var neighbors;
  //run the initial trigger_updates
  for(i = 0; i < nodes.length; i++) {
    neighbors = router_tables.get_neighbors(nodes[i]);
    neighbors.forEach(trigger_update);
  }

  //log init success
  logger.log(logger.LVL.MINIMAL, '[init] Initial topology setup complete.');
};
