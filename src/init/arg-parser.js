/********************************
*********************************
  Program:   Project One (CS4310)
  Author:    William Olson
  Date:      11/04/2015

  File:      arg-parser.js

    This file is a helper utility
    for reading arguments from the
    console, & reading  data in from
    the topology file.
*******************************
*******************************/

'use strict';

//using core modules
var fs = require('fs')      // file-system module
  , path = require('path'); // resolves relative paths

var logger = require('../util/logger'); // for help menu

//object to store input data
var _args = { opts: {} };

//some regex for pattern tests
var valid_float=/^[0-9]*[.]?[0-9]+$/g
  , valid_file=/^.*[.]txt$/g;

try { //read in console arguments

  for(var i=2; i < process.argv.length; i++){
    if(_args.opts.help) break;
    if(valid_float.test(process.argv[i])){
      _args.duration = parseFloat(process.argv[i]);//grab duration
      if(isNaN(_args.duration))
        throw new Error('[!] Error: Problem trying to parse duration arg');
      continue;
    }else if(valid_file.test(process.argv[i])){//grab txt file
      _args.filename = process.argv[i];
      _args.file_data = fs.readFileSync(path.resolve(_args.filename));
      //split buffer into a string array (1-line/element)
      _args.file_data = (_args.file_data.toString().split('\n'));
      continue;
    }else{
      switch(process.argv[i]){
        case '--color':
        case 'c':
          _args.opts.color = true;
          break;
        case '--help':
        case '-h':
          _args.opts.help = true;
          print_help();
          break;
        case '--fancy':
        case '-f':
          _args.opts.fancy = true;
          break;
        case '--debug':
        case '-d':
          _args.opts.debug = true;
          break;
        case '--verbose':
        case '-v':
          _args.opts.verbose = true;
          break;
        case '--export': // This is a debug param used for viewing topology data in an html
        case '-x':       // webpage (html viewer source code not provided for project). (KISS)
          _args.opts.export_json = true;
          break;
        case '--inject':
        case '-i':
          _args.opts.injected_filename = process.argv[i+1];
          _args.opts.inject_data = require(path.resolve(_args.opts.injected_filename));
          i++;
          break;
        default:
          //handle mashed args ex: -vfi
          if(process.argv[i][0] === '-'){
            var injected = false;
            for(var j=1; j < process.argv[i].length; j++){
              switch(process.argv[i][j]){
                case 'c':
                  _args.opts.color = true;
                  break;
                case 'h':
                  _args.opts.help = true;
                  print_help();
                  break;
                case 'f':
                  _args.opts.fancy = true;
                  break;
                case 'd':
                  _args.opts.debug = true;
                  break;
                case 'v':
                  _args.opts.verbose = true;
                  break;
                case 'x':
                  _args.opts.export_json = true;
                  break;
                case 'i':
                  _args.opts.injected_filename = process.argv[i+1];
                  _args.opts.inject_data = require(path.resolve(_args.opts.injected_filename));
                  injected = true;
                  break;
              }
            }
            if(injected) i++;
          }
      }
    }
  }
} catch (err) {
  if (err.code === 'ENOENT')
    throw new Error('[!] Error: The file ' + _args.filename + ' was not found!');
  else
    throw err;
}

//check if minimum arguments were provided
if((!_args.filename || !_args.duration) && !_args.opts.help){
  print_help();
  throw new Error('[!] Error: minimum arguments not provided!' + '\n' +
                  '[*] Please pass the name of the topology file & duration to run in seconds \n');
}

function print_help () {
  var help_str =  '\n    Usage:  node ./driver.js ./file.txt 99.9 \n\n' +
                  '    where:  \'file.txt\' is the topology file defining nodes & edges \n' +
                  '            and \'99.9\' is the duration to run the simulation in seconds.\n\n' +
                  '  Available Options: \n  -------------------------------------------- \n' +
                  '    -c, --color              show fancy tables in color  \n' +
                  '    -d, --debug              show debug (all) output  \n' +
                  '    -f, --fancy              print pretty router tables  \n' +
                  '    -h, --help               show this menu  \n' +
                  '    -i, --inject             insert events defined in a json file,  \n' +
                  '                             must be followed by a filename or path  \n' +
                  '    -v, --verbose            show verbose (medium) output  \n';
  setTimeout(function (){ logger.log(logger.LVL.MINIMAL, help_str); }, 100);
}

module.exports = {
  get_input: function () {
    return _args;
  }
};
