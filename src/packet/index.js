/*************************************************************
**************************************************************
  Program:  Project One (CS4310)
  Author:   William Olson
  Date:     11/04/2015

  File:     Packet (packet/index.js)

  Description:
    Producer module for creating new Packets.
**************************************************************
**************************************************************/

  'use strict';

  var config = require('../network/config')
    , packet_handler = require('./packet-handler');

 /**
   * Constructor: Packet
   *   should be called with the 'new' keyword.
   * @param: type
   *   the EVENT_TYPES type
   * @param: src
   *   the creator of the event.
   * @param: dest
   *   the destination the event happens at.
   * @param: eta
   *   the calculated time when this event will
   *   be processed.
   * @param: content
   *   an Object that holds the packet data,
   *   including dv_table if type DV_PACKET.
   *   should contain at minimum:
   *     og_src, og_dest, data
     **/
  function Packet(type, src, dest, eta, content) {


    //sections
  	this.header = {};
  	this.payload = {};

    //allow allready structued packets
    if(typeof type === 'object')
      return Packet.Clone(this, type);

    //generic properties
  	this.id = ++config.event_count;
  	this.type = type;
  	this.eta = eta;
  	this.header.src = src;
  	this.header.dest = dest;
  	this.payload.data = content.data;
  	this.header.og_src = content.og_src;
  	this.header.og_dest = content.og_dest;

      //conditional properties
  	if (type === config.EVENT_TYPES.DV_PACKET){
  		this.payload.table = content.table;
      config.dv_packet_count++;
    }else if(type === config.EVENT_TYPES.DATA_PACKET){
      config.data_packet_count++;
    }

  }

  Packet.Clone = function (new_packet, og_packet) {

    new_packet.id = og_packet.id || ++config.event_count; //can take custom id
    new_packet.type = og_packet.type;
    new_packet.eta = og_packet.eta;
    new_packet.header.src = og_packet.header.src;
    new_packet.header.dest = og_packet.header.dest;
    new_packet.payload.data = og_packet.payload.data;
    new_packet.header.og_src = og_packet.header.og_src;
    new_packet.header.og_dest = og_packet.header.og_dest;
    if (og_packet.payload.table)
      new_packet.payload.table = og_packet.payload.table.slice();

    //stats update
    if (new_packet.type === config.EVENT_TYPES.DV_PACKET){
  		//this.payload.table = og_packet.payload.table;
      config.dv_packet_count++;
    }else if(new_packet.type === config.EVENT_TYPES.DATA_PACKET){
      config.data_packet_count++;
    }
    if(og_packet.id) config.event_count++; //custom id -> don't forget to tick count

    return new_packet;
  };


 /**
   * to_string:
   *   produces a printable version of the
   *   packet object.
   **/
  Packet.prototype.to_string = function () {
    return ('[*]   (' + this.id + ') ' + this.type +
             ':msg:' + this.view_data() +
             ' Arrived from Node ' + this.header.src +
             ' to Node ' + this.header.dest +
             ' (@time: ' + config.get_time() + ')');
  };


 /**
   * process: , throws error
   *   handles processing of events (packet arrivals).
   *   (See packet-handler.js for logic).
   **/
  Packet.prototype.process = function () {
  	 try{
       packet_handler.process_packet(Packet, this);
     } catch (err){
       throw err; // bubble up any errors
     }
  };

 /**
   * view_data:
   *   peeks at packet data and returns
   *   what is visible to header.dest node.
   * @return: String
   *   the string containing payload data
   *   of the packet if visible, otherwise
   *   returns a string containing '<encrypted>'.
   **/
  Packet.prototype.view_data = function () {
    if(this.type === config.EVENT_TYPES.DATA_PACKET){
      return ((this.header.dest === this.header.og_dest) ? ('<'+this.payload.data+'>') : '<encrypted>');
    }else {
      return '<' + this.payload.data + '>';
    }
  };



  module.exports = Packet;
