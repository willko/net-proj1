/*************************************************************
**************************************************************
  Program:  Project One (CS4310)
  Author:   William Olson
  Date:     11/04/2015

  File:     packet-handler.js

  Description:
    The worker module for processing packet arrivals.
**************************************************************
**************************************************************/

'use strict';


var config = require('../network/config')
  , routers = require('../router-tables')
  , p_queue = require('../util/p-queue')
  , logger = require('../util/logger');



module.exports = {

/**
  * process_packet:
  *   handles processing/delegation of packet
  *   arrival tasks.
  * @param: Packet {Object}
  *   the Packet module Object constructor
  * @param: packet {Object}
  *   the packet Object that has arrived and
  *   is being processed.
  **/
  process_packet: function (Packet, packet) {

    //  >>~-| General |-->
    // -------------------

    // ---LOG_ARRIVAL---
    logger.log(logger.LVL.LOUD, packet.to_string());

    // ---FORWARD_PACKET---
    if(packet.header.dest !== packet.header.og_dest){
      forward_packet(packet);
      return;
    }


    //  >>~-| Specific Types |-->
    // --------------------------

    switch(packet.type){
      case config.EVENT_TYPES.LINK_FAILURE:
        //handle LINK_FAILURE events just like
        //dv_packet that advertise a cost of -1
      case config.EVENT_TYPES.DV_PACKET:
        var update_made = false;

        //compare dv_table and update if necessary
        if(packet.header.src != packet.header.og_src){
          update_made = routers.update(packet.header.dest, packet.payload.table
                                      , packet.header.src, config.get_time()
                                      , packet.header.og_src); //pass og_src
        } else{
          update_made = routers.update(packet.header.dest, packet.payload.table
                                      , packet.header.src, config.get_time());
        }
        if(update_made){
          routers.trigger_update(Packet, packet, p_queue);
          config.convergence_time = config.clock;
          config.last_updated_table = packet.header.og_dest;
        }

        break;
      case config.EVENT_TYPES.DATA_PACKET:
        //pakets are logged above this switch statement
        //nothing else to do with data packets on arrival
        break;
      default:
        throw new Error('[!] Error: the event of type ' + this.type + ' could not be processed!');
  	}
  }

};


/**
  * forward_packet:
  *   forwards the packet torward
  *   it's ultimate destination.
  * @param: packet
  *   the packet to forward. reuses
  *   the same packet Object instead
  *   creating a new one.
  **/
function forward_packet (packet) {
  //FORWARD ONWARD
  logger.log(logger.LVL.LOUD, '[*]       >> FORWARDING PACKET ultimate path is from Node ' +
              packet.header.og_src + ' to Node ' + packet.header.og_dest);
  packet.header.src = packet.header.dest;
  var route = routers.get_route(packet.header.src, packet.header.og_dest);
  if(route.cost === -1){
    logger.log(logger.LVL.LOUD, '[*]       !>> DROPPING PACKET can\'t route to Node ' + route.dest +
               ' through Node ' + route.hop + ', link is down');
    return;
  }
  packet.header.dest = route.hop;
  packet.eta = config.clock + config.get_delay(packet.header.src, route.hop);
  p_queue.push(packet);
  config.forward_count++;
}