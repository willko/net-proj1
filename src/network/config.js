/********************************
*********************************

  Program:   Project One (CS4310)
  Author:    William Olson
  Date:      11/04/2015

  File:      config.js

*********************************
*********************************/

'use strict';


module.exports = {
      clock: 0.000000000
    , duration: 0
    , step:  0.000001000 //step in micro-seconds
    , step_count: 0
    , action_count: 0
    , event_count: 0
    , dv_packet_count: 0
    , data_packet_count: 0
    , forward_count: 0
    , links: {} //gets populated in init module
    , EVENT_TYPES: {
        DV_PACKET: 'DV_PACKET'
      , DATA_PACKET: 'DATA_PACKET'
      , LINK_FAILURE: 'LINK_FAILURE'
    }
    /*
     * get_time:
     *   returns the time rounded
     *   close to the given unit.
     * @param: opt_s_m
     *   optional string parameter
     *   containing either 's' for
     *   seconds or 'm' for msecs.
     * @return: String
     *   the rounded clock time as a string.
    */
    , get_time: function (opt_s_m) {
      if(opt_s_m){
        opt_s_m = opt_s_m.toLowerCase();
        if(opt_s_m === 's'){
          return this.clock.toFixed(1);
        }else {
          return this.clock.toFixed(4);
        }
      }else{
        return this.clock.toFixed(7);
      }
    }
    /*
     * log_links:
     *   prints the structured link data
     *   read in from the topology file.
     *   this is called for debugging
     *   purposes only.
    */
    , log_links: function () {
      console.info('LINKS:');
      console.log('------------------------------------');
      console.dir(this.links);
      console.log('------------------------------------');
    }
    /*
     * get_delay: , throws error
     *   a helper utility method for
     *   calculating the delay times.
    */
    , get_delay: function (src, dest) {
      var found = false, delay = -1.0;
      //search for path cost
      this.links.forEach(function (link){
        if(link.src === src && link.dest === dest){
          found = true;
          delay = link.delay;
        } else if (link.src === dest && link.dest === src){
          found = true;
          delay = link.delay;
        }
      });
      if(!found)
        throw new Error('[!] Error: No delay found: There is not a path between node ' + src + ' & ' + dest);
      return delay;
    }
};
