/********************************
*********************************

  Program:   Project One (CS4310)
  Author:    William Olson
  Date:      11/04/2015

  File:      network.js

  Description:
    High level business logic controller module.
    Handles the control flow of the program.

 * network.js:
 *   Provides an object with methods for invoking the
 *   3 phases of running the project: setup, start, & log.
 *   Also handles running periodic updates.
 *
 * @return: {Object}
 *   the Object for running the program.
 *   it has the following properties:
 *     @property: setup
 *       method for performing all initialization.
 *     @property: start
 *       method for running the simulation's while loop.
 *     @property: log
 *       method for printing the router_table results.
 *

*********************************
*********************************/

'use strict';

//import my project modules
var config = require('./config')
  , init = require('../init')
  , router_tables = require('../router-tables')
  , Packet = require('../packet')
  , p_queue = require('../util/p-queue')
  , logger = require('../util/logger');


/*
  define this module
*/
module.exports = {

/**
  * setup:
  *   invokes the init module for
  *   initializing the simulated
  *   network topology.
  **/
  setup: function () {
    logger.log(logger.LVL.MINIMAL, '[network] Starting Networks Project 1');
    init();
    return this; //allow chaining
  }

/**
  * start:
  *   runs the simulation while loop
  *   processing events/packets until
  *   the duration console arg has been
  *   reached by the config.clock.
  **/
  , start: function () {

    //exit if help menu requested
    if(config.opts.help) return this;

    //array for events that need processing
    var current_events;

    //start the simulation
    while (config.clock < config.duration) {
      //check queue for scheduled events that should
      //be processed at this time.
      current_events = p_queue.get_events(config.clock);
      if(current_events.length > 0){
        logger.log(logger.LVL.LOUD, '[*] Processing Events:');
        for(var i=0; i < current_events.length; i++){
          config.clock = current_events[i].eta;
          current_events[i].process(); //proccess the event
          config.action_count++;
        }
      }
      //schedule periodic update if needed
      if(config.step_count !== 0 && config.step_count % 1000000 === 0){
        periodic_update();
      }
      //increment clock & stats
      config.clock += config.step;
      config.step_count++;
    }

    return this;
  }


/**
  * log:
  *   outputs the router tables to the
  *   console (along with stats if -v
  *   flag is given).
  **/
  , log: function () {
      if(config.opts.help) return;
      //show tables
      logger.log(logger.LVL.MINIMAL, router_tables.to_string(config.opts.fancy, config.opts.color));
      //and end stats
      print_stats();
      process.on('exit', function() {
        logger.log(logger.LVL.MINIMAL, '[network] End of program.');
      });
  }

 }; //end of module.exports


 /**
   * periodic_update:
   *   creates dv_packet events for all nodes.
   *   is called every 1 second from network.start().
   * note:
   *   First periodic update occurs when config.clock
   *   reaches 1 second (not at time 0).
   **/
  function periodic_update () {

    //get nodes and create some tmp variables
    var nodeArr = router_tables.get_nodes()
      , arrival_time, neighbor_entries, dv_packet;

    //step through all nodes
    for(var i=0; i < nodeArr.length; i++){

      //get an array of the node's neighbors and send dv_packets to each one
      neighbor_entries = router_tables.get_neighbors(nodeArr[i]);
      for(var j=0; j < neighbor_entries.length; j++){
        logger.log(logger.LVL.CHATTY, '[*] Scheduling Periodic update from Node ' + nodeArr[i] +
            ' to Node ' + neighbor_entries[j].dest + ' (@time: ' + config.get_time('s') + ')');

        //create a new packet/event (periodic_update) and add it to the queue
        var route = router_tables.get_route(nodeArr[i], neighbor_entries[j].dest);
        arrival_time = (config.clock + config.get_delay(nodeArr[i], route.hop));
        dv_packet = new Packet(
                            config.EVENT_TYPES.DV_PACKET
                          , nodeArr[i]
                          , route.hop
                          , arrival_time
                          , {
                              data: 'PERIODIC_UPDATE'
                              , og_src: nodeArr[i]
                              , og_dest: neighbor_entries[j].dest
                              , table: router_tables.get_dv_table(nodeArr[i], neighbor_entries[j].dest)
                            }
                          );
        p_queue.push(dv_packet);
      }

    }
  }

function print_stats() {
  logger.log(logger.LVL.LOUD, '[*] END_STATS: (doesn\'t include init changes)');
  logger.log(logger.LVL.LOUD, '[*] -----------------------------------------');
  logger.log(logger.LVL.LOUD, '[*] end.clock             = ' + config.clock);
  logger.log(logger.LVL.LOUD, '[*] end.ticks             = ' + config.step_count);
  logger.log(logger.LVL.LOUD, '[*] end.actions           = ' + config.action_count);
  logger.log(logger.LVL.LOUD, '[*] last.update_time      = ' + config.convergence_time);
  logger.log(logger.LVL.LOUD, '[*] last.router_updated   = ' + config.last_updated_table);
  logger.log(logger.LVL.LOUD, '[*] packets.created       = ' + config.event_count);
  logger.log(logger.LVL.LOUD, '[*]     data_packets      = ' + config.data_packet_count + '/'  + config.event_count);
  logger.log(logger.LVL.LOUD, '[*]     dv_packets        = ' + config.dv_packet_count + '/'  + config.event_count);
  logger.log(logger.LVL.LOUD, '[*] re_used.packets       = ' + config.forward_count + ' (forwarded)');
                               //+ '=?=' + ((config.action_count + p_queue.get_size()) - config.event_count) + ' (forwarded)'); //same
  logger.log(logger.LVL.LOUD, '[*] packets_still_in_Q    = ' + p_queue.get_size());
  logger.log(logger.LVL.LOUD, '[*] -----------------------------------------');
}
