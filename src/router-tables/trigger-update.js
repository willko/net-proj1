/********************************
*********************************
  Program:   Project One (CS4310)
  Author:    William Olson
  Date:      11/04/2015

  File:      trigger-update (router-tables/trigger-update.js)

    For processing trigger updates.
*******************************
*******************************/


'use strict';


var config = require('../network/config')
  , logger = require('../util/logger');

/**
  * trigger_update:
  *   Called when a DV_PACKET arrives and a
  *   routers table has been modified with
  *   changes. Sends new trigger updates and
  *   uses split horizon algorithm.
  * @param: Packet
  *   the Packet module Object (constructor).
  * @param: packet
  *   the incoming packet Object that
  *   is being processed. (the DV_PACKET
  *   that caused the update to occur).
  * @param: p_queue
  *   the p-queue module API Object
  *   for scheduling events.
  **/
module.exports = function (Packet, packet, p_queue) {

  var router_tables = this;
  var targets = router_tables.get_neighbors(packet.header.dest);

  //step through neighbor nodes
  for(var i=0; i < targets.length; i++){

    if(targets[i].dest === packet.header.og_src){ //use split horizon
      logger.log(logger.LVL.CHATTY, '[*]    ~  Skipping Trigger packet for Node ' +
                  targets[i].dest + ' (split horizon)');
      continue;
    }else{
      logger.log(logger.LVL.CHATTY, '[*]    -> Sending Trigger Update from Node ' +
                  packet.header.dest + ' to Node ' + targets[i].dest);
      //get dv_table & determine packet data
      var dv_table, eta, route, dv_packet;
      dv_table = router_tables.get_dv_table(packet.header.dest, targets[i].dest);
      route = router_tables.get_route(packet.header.dest, targets[i].dest);
      eta = config.clock + config.get_delay(packet.header.dest, route.hop);

      // create a dv_packet and send it
      dv_packet = new Packet( config.EVENT_TYPES.DV_PACKET
                            , packet.header.dest
                            , route.hop
                            , eta
                            ,{
                                og_src: packet.header.dest
                              , og_dest: targets[i].dest
                              , data: 'TRIGGER_UPDATE'
                              , table: dv_table
                            });
      p_queue.push(dv_packet);
    }

  } //end looping neighbors

};