/********************************
*********************************
  Program:   Project One (CS4310)
  Author:    William Olson
  Date:      11/04/2015

  File:      update (router-tables/update.js)

    This file is for managing router
    table updates.
*******************************
*******************************/


'use strict';



//grab needed modules
var logger = require('../util/logger');
var routers;


var INFINITY = -1;


  /**
    * path_cost:
    *   returns the cost field for node dest in
    *   router rid's table.
    **/
  var path_cost = function (rid, dest) {
    //lookup dest in router table and return cost.
    for(var i =0; i < routers[rid].table.length; i++){
      if(routers[rid].table[i].dest === dest){
        return routers[rid].table[i].cost;
      }
    }
    //returns 0 if no entry found. This only happens when a neighbor
    //node has not been added to a router's table yet. Returning 0
    //alows correct costs to be initially set for the neighbor.
    return 0;
  };
  /*
    update
  */
  function update (router_service, rid, new_table, next_hop, time, opt_og_src) {
      //router_service = this; //Bound by 'router-tables/index.js' router_service
      routers = router_service.get_routers();
      var updated = false
        , match = {found: false}
        , total_cost
        , hop_cost
        , src_node;

      //get the original sender (short circuit assign)
      //set to opt_og_src if passed in, else next_hop
      src_node = opt_og_src || next_hop;

      if(time > 0.0910){
        time = time;
      }

      //add node if router table not found
      if(routers[rid] === undefined){
        logger.log(logger.LVL.CHATTY, '[*] +NODE ADDED TO NETWORK: ' + rid + ' (@time: ' + time + ')');
        routers[rid] = { table: [] };
      }

      //step through dv_table and update matches
      for(var i = 0; i < new_table.length; i++){
        match.found = false;
        for(var j = 0; j < routers[rid].table.length; j++){
          if(routers[rid].table[j].dest === new_table[i].dest){
            match.found = true;
            // -------------- MODDIFY ENTRY ----------------

            // get pre_cost
            if(opt_og_src)
              hop_cost = path_cost(rid, opt_og_src);
            else
              hop_cost = path_cost(rid, next_hop);
            if( //let updates happen when the following 2 conditions are true
              (routers[rid].table[j].cost > (new_table[i].cost + hop_cost)) ||
              (routers[rid].table[j].cost === INFINITY && new_table[i].cost != INFINITY)
              ){
              updated = true;

              //update the cost for entry in table
              if(hop_cost === INFINITY || (new_table[i].cost === INFINITY && next_hop === routers[rid].table[j].hop)){
                routers[rid].table[j].cost = INFINITY;
                routers[rid].table[j].hop = next_hop;
                logger.log(logger.LVL.LOUD, '[*]   Updating Router ' + rid + '\'s Table entry for node ' +
                new_table[i].dest + ' to INFINITY (@time: ' + time + ')');
              }else{
                routers[rid].table[j].cost = (new_table[i].cost + hop_cost);
                routers[rid].table[j].hop = next_hop;
                logger.log(logger.LVL.LOUD, '[*]   Updating Router ' + rid + '\'s Table entry for node ' +
                  new_table[i].dest + ' to ' + (new_table[i].cost + hop_cost) + ' (@time: ' + time + ')');
              }
            }
          }
        }
        if(match.found === false){
          updated = true;
          // -------------- ADD ENTRY ----------------

          //determine the new cost of the route
          total_cost = -1;
          hop_cost = path_cost(rid, next_hop);
          if(hop_cost === INFINITY || new_table[i].cost === INFINITY){
            logger.log(logger.LVL.LOUD, 'SETTING TOTAL_COST TO INFINITY! ' + INFINITY);
            total_cost = INFINITY;
            //could trigger a function to call setTimeout
            //of removal if still -1 after timeout period
          } else {
            total_cost = hop_cost + new_table[i].cost;
          }
          //add the new entry to the table
          routers[rid].table.push({
              dest: new_table[i].dest
            , cost: total_cost
            , hop: next_hop
          });

          logger.log(logger.LVL.LOUD, '[*]   Updating Router ' + rid + '\'s Table *NEW entry { ' +
                new_table[i].dest + ', ' + total_cost + ', ' + next_hop + ' } (@time: ' + time + ')');

        }
      }
      return updated;
    }

    //expose update function
    module.exports = update;
