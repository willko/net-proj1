/********************************
*********************************

  Program:   Project One (CS4310)
  Author:    William Olson
  Date:      11/04/2015

  File:      router-tables (router-tables/index.js)

    This file is for managing router
    tables.

    @return: router_service
      returns an Object that provides
      methods for interacting with the
      router tables.  See return @ EOF
      and method descriptions below.
      @property: update
        defined as _update below, handles
        incoming dv_table data comparisons.
      @property: get_neighbors
      @property: get_nodes
      @property: dv_table
      @property: to_string

*******************************
*******************************/
'use strict';



//holds all router tables
var routers = require('./routers');

//update handler abstracted to file
var _update = require('./update')
  , _trigger_update = require('./trigger-update');


//the exported object
var router_service = {};

//attach to_string
router_service.to_string = routers.to_string;


/**
  *  get_nodes
  *     gets all nodes on network.
  *  @return: [Array]
  *     an array containing string entries
  *     which are the ids of all nodes.
  **/
router_service.get_nodes = function () {
  var results = [];
  Object.keys(routers).forEach(function (key) {
    if((/[0-9]+/g).test(key))
      results.push(key);
  });
  return results;
};

//satisfy: router_tables.add_neighbors(nodes[i], router_tables.get_table(nodes[i]));

/**
  * get_table:
  *   simply returns the router table for
  *   node with id rid.
  * @param: rid
  *   the router id (string) of the desired
  *   table.
  * @return: [Array]
  *   copy of the array containing router
  *   table entry objects.
  **/
router_service.get_table = function (rid) {
  if(routers[rid] && /[0-9]+/g.test(rid))
    return routers[rid].table.slice();
};
/**
  * add_neighbors:
  *   adds the passed in neighbors to
  *   router with id rid's neighbor array.
  * @param: rid
  *   the node id (string) of the router that
  *   the neighbors are added to
  * @param: table
  *   the array containing objects with dest
  *   properties, Ex:
  *      [ {dest: '1'}, {dest: '2}, ...etc. ]
  **/
router_service.add_neighbors = function (rid, table) {
  if(routers[rid] === undefined)
    throw new Error('[!] Error: cant add neighbors. Node ' + rid + ' does not exist');
  routers[rid].neighbors = routers[rid].neighbors || [];
  table.forEach(function (item) {
    var found = false;
    for(var i=0; i < routers[rid].neighbors.length; i++){
      if(routers[rid].neighbors[i].dest)
        if(routers[rid].neighbors[i].dest === item.dest){
          found = true;
          routers[rid].neighbors[i].state = 'UP';
        }
    }
    if(!found){ routers[rid].neighbors.push({dest: item.dest, state: 'UP'}); }
  });
};

/**
  *  get_neighbors:
  *     gets an array of neighbor nodes.
  *  @param: rid
  *     the router id (string) of the node
  *     that has neighbors to get.
  *  @return:
  *     the array containing objects which
  *     have only 2 properties: dest & state
  *     (both strings) where state is UP or DOWN.
  **/
router_service.get_neighbors = function (rid) {
  if(routers[rid])
    return routers[rid].neighbors;
};

/**
  *  get_dv_table: , throws error
  *    produces a shortened router table for
  *    sending dv_packet (deletes hop field,
  *    and entry for dest or dv_packet).
  *  @param: rid
  *     the router id (string) of the router
  *     sending the dv_packet.
  *  @param: dest
  *     the router id (string) of the node
  *     that will receive the dv_packet.
  *  @return: [Array]
  *     an array containing table entry objects
  *     excluding the hop field. (dest) Entry is
  *     also ommitted.
  **/
router_service.get_dv_table = function (rid, dest) {
  var dv_table = [];
  if(routers[rid] === undefined || routers[rid].table === undefined) {
    throw new Error('[!] Error: Trying to produce a dv_table failed: Router ' + rid + '\'s Table doesn\'t exist');
  }
  for(var entry=0; entry < routers[rid].table.length; entry++)
    if(routers[rid].table[entry].dest !== dest)
      dv_table.push({dest: routers[rid].table[entry].dest, cost: routers[rid].table[entry].cost});
  return dv_table;
};

/*
  _get_route: , throws error
    consults router table and returns
    the entry with the matching dest.
  @param: rid
     the router id (string) of the router
     consulting its table.
  @param: dest
     the node id (string) of the node
     to get to (ultimate dest).
  @return: [Object]
     the object which is the entry from the
     routers table.
*/
router_service.get_route = function (rid, dest) {
  if(routers[rid] === undefined || routers[rid].table === undefined)
    throw new Error('[!] Error: Trying to get route: Router ' + rid + '\'s Table doesn\'t exist');
  for(var entry=0; entry < routers[rid].table.length; entry++)
    if(routers[rid].table[entry].dest === dest)
      return routers[rid].table[entry];
};

//bind the router_service to the this variable in trigger-update
//see: router-tables/trigger-update.js for details
router_service.trigger_update = function (Packet, packet, p_queue) {
  _trigger_update.apply(router_service, [Packet, packet, p_queue]);
};
//pass router_service to update module & expose less params
//see: router-tables/update.js for details
router_service.update = function (rid, new_table, next_hop, time, opt_og_src){
  return _update(router_service, rid, new_table, next_hop, time, opt_og_src);
};



//allow direct access if needed (use this carefully)
router_service.get_routers = function (){
  return routers;
};


module.exports = router_service;
