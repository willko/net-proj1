/********************************
*********************************
  Program:   Project One (CS4310)
  Author:    William Olson
  Date:      11/04/2015

  File:      routers.js

    For defining the routers Object.
*******************************
*******************************/

'use strict';

/**
  *  routers:
  *    This file defines the routers object
  *    and to_string method for printing.
  **/
var routers = {};

/**
  * to_string: , throws error
  *   returns all router_table data in one string.
  * @param: opt_fancy
  *   optional boolean parameter for determining
  *   if output should be formatted into nice tables.
  * @param: opt_color
  *   optional boolean parameter to determine if
  *   output should be in color.
  **/
routers.to_string = function (opt_fancy, opt_color) {
  if(opt_fancy) return routers.to_fancy_string(opt_color);
  var raw_str = '\nROUTER_TABLES:\n';
  raw_str += '\n------------------------------------\n';
  Object.keys(routers).forEach(function (key) {
    if(!routers[key].table) return;
    raw_str +=  '\n    --  Router #' + key + '\'s Table  --  \n';
    raw_str += '\tdest  \tcost  \thop\n';
    for(var entry = 0; entry < routers[key].table.length; entry++){
      raw_str += '\t' + routers[key].table[entry].dest + ', ';
      raw_str += '\t' + routers[key].table[entry].cost + ', ';
      raw_str += '\t' + routers[key].table[entry].hop;
      raw_str += '\n';
    }
    raw_str += '\n';
  });
  raw_str += '\n------------------------------------\n';
  return raw_str;
};

/**
  * to_fancy_string: , throws error
  *   returns all router_table data as a string
  *   formatted with the cli-table npm module.
  * @param: opt_color
  *   optional boolean parameter for determining
  *   whether or not to format string with color.
  **/
routers.to_fancy_string = function (opt_color){
  var result = '';
  try{
    require.resolve('cli-table');
    var Table = require('cli-table');
    var tablesArr = [], rows, head;
    var row_opts = {
              head: ['dest', 'cost', 'hop']
            , colWidths: [12,12,12]
     }, head_opts = {
              head: ['']
            , colWidths: [42]
     };

     if(opt_color){
       head_opts.style = row_opts.style = { head: ['yellow'], border: ['green'] };
     }else{
       head_opts.style = row_opts.style = { head: ['white'], border: ['gray'] };
     }

    //loop and populate tablesArr
    Object.keys(routers).forEach(function (rid){
      if(!routers[rid].table) return;
      head_opts.head = ['-- Router #' + rid + '\'s Table --'];
      head = new Table(head_opts);
      rows = new Table(row_opts);
      for(var i=0; i < routers[rid].table.length; i++)
        rows.push([
            routers[rid].table[i].dest
          , routers[rid].table[i].cost
          , routers[rid].table[i].hop]);
      head.push([rows.toString()]);
      tablesArr.push(head);
    });

    //concat tableArr into 1 string
    result += '\nROUTER_TABLES:\n';
    result += '\n------------------------------------\n';
    for(var i=0; i< tablesArr.length; i++)
      result += tablesArr[i] + '\n';
    result += '\n------------------------------------\n';
  }catch(exc){
    throw new Error('[!] ERROR: ' + exc.toString() + '\n' +
                    '[!] Error: An npm package is missing: cli-table' + '\n' +
                    '[!] Error: Can\'t print pretty tables without cli-table.' + '\n' +
                    '[!] Hint: Try running \'npm install\' in the package.json directory.');
  }
  return result;
};



//return routers object
module.exports = routers;
