/********************************
*********************************

  Program:   Project One (CS4310)
  Author:    William Olson
  Date:      11/04/2015

  File:      p-queue.js

*********************************
*********************************/

'use strict';



  var event_queue; //the array that holds events
  if(event_queue === undefined) event_queue = [];

  /*
   * _sort_queue:
   *   sorts the event_queue array
   *   by eta values.
  */
  var _sort_queue = function () {
    event_queue.sort(function (a, b) {
      return (a.eta > b.eta) ? 1 : ( (a.eta < b.eta) ? -1 : 0 );
    });
  };

  module.exports = {

   /**
     * push:
     *   pushes an object to the
     *   event_queue (then sorts the queue).
     * @param: obj
     *   the event object to add to queue.
     **/
    push: function (obj){
      event_queue.push(obj);
      _sort_queue();
    }

   /**
     * pop:
     *   pops the latest event off
     *   the event_queue (removes it).
     * @return: {Object}
     *   the object at index 0 in event_queue.
     **/
    , pop: function (){
      return event_queue.shift();
    }

   /**
     * get_events:
     *   returns all events in the
     *   event_queue that should be
     *   processed at the given time.
     * @param: time
     *   float that contains the time
     *   to filter events returned.
     * @return: Array
     *   the array of events that have an
     *   eta property that is greater
     *   than or equal to the time param.
     **/
    , get_events:     function (time) {
      var results = [];
      while(event_queue.length > 0 && event_queue[0].eta <= time) {
        results.push(this.pop());
      }
      return results;
    }

   /**
     * log_all:
     *   simple print method for displaying
     *   the event_queue.
     **/
    , log_all: function () {
      console.log('P-Queue:');
      console.log('----------------------');
      console.dir(event_queue);
      console.log('----------------------');
    }

   /**
     * get_size:
     *   get the number events in the
     *   event_queue.
     **/
    , get_size: function () {
      return event_queue.length;
    }

  };