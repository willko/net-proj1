'use strict';

var config = require('../network/config');


module.exports = {
    LVL: {
          CHATTY:  0
        , LOUD:    1
        , MINIMAL: 2
      }
   /**
     * log:
     *   outputs the msg param to
     *   the console if verbosity
     *   privelege level is equal to
     *   the options passed to the 
     *   program.
     * @param: verbosity_level
     *   a LVL type (int) that
     *   represents the wanted
     *   verbosity level.
     * @param: msg
     *   the string to output.
     **/
  , log: function (verbosity_level, msg) {
      switch(verbosity_level){
        case this.LVL.MINIMAL: // always shows this LVL
          console.log(msg);
          break;
        case this.LVL.LOUD: // flags: -v or -d shows these
          if(config.opts)
            if(config.opts.verbose || config.opts.debug) 
              console.log(msg);
          break;
        case this.LVL.CHATTY:  // flag: -d show these msgs
          if(config.opts && config.opts.debug) 
            console.log(msg);
          break;
        default:
          return;
      }
  }
};