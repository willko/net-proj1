#!/usr/bin/env node
/*************************************************************
**************************************************************

  Program: Project One (CS4310)
  Author: William Olson
  Date: 11/04/2015

  File: driver.js

  Description:
    Runs the program. Takes a filename and
    duration in seconds.

**************************************************************
**************************************************************/


'use strict';


try{ //run simulator
  require('./network').setup().start().log();
} catch (error){
  console.error('[!]' + error.toString());
}
